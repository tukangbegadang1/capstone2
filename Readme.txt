Latar Belakang
Departemen Kepolisian Boston (BPD) merekam dan membuat laporan insiden kejahatan disediakan untuk mendokumentasikan detail awal seputar insiden yang ditanggapi oleh petugas BPD. Dengan data ini diharapkan kepolisian dapat meningkatkan kinerja di suatu wilayah di boston untuk menekan angka kejahatan.

Pernyataan Masalah
BPD ingin mengetahui daerah mana saja di boston yang memiliki kasus kejahatan narkoba paling banyak selama periode 2015 - 2018 ?

Video : https://drive.google.com/file/d/1h11OIJoPjwxcg_uzxYp-YsPFWRQq20UO/view?usp=share_link
Tableau : https://public.tableau.com/app/profile/andrian.ashari/viz/Boston_Crime_16806223168730/Story1?publish=yes
